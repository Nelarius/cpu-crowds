#ifndef NAVIGATIONGRID_H
#define NAVIGATIONGRID_H

#include <core/ISerializable.h>
#include <utils/ThreadPool.h>
#include <core/Agent.h>
#include <core/DirectionalNode.h>
#include <utils/Buffer.h>
#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <cmath>    // for abs

typedef ce::DirectionalNode<float>  DirectionalNodef;

namespace ce {

/// \brief The data grid agents use for navigation.
///
/// A mesh grid might help complement this one, it would store the height, from
/// which some of the data for this grid could be obtained.
class NavigationGrid : public ISerializable {
    public:
        NavigationGrid( const ObjectNode& );
        ~NavigationGrid();

        void initialize();
        void update();

        /// \brief Get the ObjectNode representation of this object.
        virtual std::shared_ptr<ObjectNode> nodeRepresentation() const override;

        const std::vector<float>&       getDensitySum() const;
        const std::vector<float>&       getDiscomfortSum() const;
        const std::vector<glm::vec2>    getAverageVelocity() const;

        size_t  getXNodeCount() const;
        size_t  getYNodeCount() const;

    private:
        // DATASTRUCTURES
        struct Tile {
            // TODO: change Buffer so that this constructor can be
            // deleted
            Tile() = delete;
            Tile(
                std::size_t cStart,
                std::size_t cEnd,
                std::size_t rStart,
                std::size_t rEnd
            )
            :   colStart( cStart ),
                colEnd( cEnd ),
                rowStart( rStart ),
                rowEnd( rEnd ),
                isConverged( false )
                {}
            ~Tile() = default;

            std::size_t colStart;
            std::size_t colEnd;
            std::size_t rowStart;
            std::size_t rowEnd;
            bool isConverged;
        };

        // METHODS
        // get the cell, given two coordinates
        inline size_t   getCell_( const glm::vec2& p ) const {
            size_t j = floor( p.x / dx_ );
            size_t i = floor( p.y / dy_ );
            return nx_ * i + j;
        }
        // get the cell, given a row and column index
        inline size_t   getCell_( size_t row, size_t col ) const {
            return nx_ * row + col;
        }
        // get the coordinates, given a cell index
        inline glm::vec2   getCoords_( size_t index ) const {
            size_t j = index % nx_;
            size_t i = index / nx_;
            return glm::vec2( j*dx_, i*dy_ );
        }
        inline size_t   getCol_( size_t index ) const {
            return index % nx_;
        }
        inline size_t   getRow_( size_t index ) const {
            return index / nx_;
        }

        // get a scalar value, or zero if index out of range
        glm::vec2   getVec2OrZero_( std::size_t, std::size_t, const std::vector<glm::vec2>& ) const;
        float       getScalarOrInfinity_( std::size_t, std::size_t, const std::vector<float>& ) const;

        // for calculating the cost field
        void        splatDiscomfort_();
        void        computeDensitySumVelocityDensitySum_(); // runs in parallel!
        void        computeAverageVelocity_();              // runs in parallel! require _velocityDensitySum
        void        computeFlowSpeed_();                    // runs in parallel! require _averageVelocity
        void        computeSpeed_();        //runs in parallel! require _flowSpeed
        void        computeCost_();         //runs in parallel! require _speedField

        // for calculating the potential field
        void        solvePotential_();	// runs in parallel! require cost_
        void        markGoalTiles_();
        void        removeConvergedTiles_();
        void        update1Connected( Tile& t );
        float       solveQuadratic_( float px, float py, float cx, float cy );
        void        initializePotentialGrid_();  //set goal points to zero, infinity elsewhere
        void        tileize_();  // divide the grid up into tiles
		void 		updateCell_( std::size_t row, std::size_t col );
        void        innerLoop_( Tile& t );
        void        convergenceReduction_( Tile& t );

        // for updating the agents
        void        agentMover_();

        // FIELDS
        ThreadPool      threadPool_;

        const float         dimx_;
        const float         dimy_;
        const std::size_t   nx_;
        const std::size_t   ny_;
        const float         dx_;
        const float         dy_;
        const float         timeStep_;
        const std::size_t   agentCount_;
        const float         agentRadius_;
        const float         discomfortRadius_;
        const float         maxDensity_;
        const float         minDensity_;
        const float         maxSpeed_;
        const float         pathWeight_;
        const float         timeWeight_;
        const float         discomfortWeight_;
        const float         goalRadius_;
        const std::size_t   innerLoopIterations_;
        const float         convergenceEpsilon_;
        const std::size_t   tileSize_;
        glm::vec2           goalPoint_;

        std::vector<Agent>  agents_;    //figure out a way to align these

        // cost field buffer
        std::vector<float>              discomfortSum_;
        std::vector<float>              densitySum_;
        std::vector<glm::vec2>          velocityDensitySum_;
        std::vector<glm::vec2>          averageVelocity_;
        std::vector<DirectionalNodef>   flowSpeed_;
        std::vector<DirectionalNodef>   speed_;
        std::vector<DirectionalNodef>   cost_;

        // potential field buffer
        std::vector<float>              potential_;
        std::vector<float>              oldPotential_;
        std::vector<Tile>               tileList_;
        ce::Buffer<Tile>                activeList_;


};

}

#endif // NAVIGATIONGRID_H
