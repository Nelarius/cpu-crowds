#ifndef BUFFER_H_INCLUDED
#define BUFFER_H_INCLUDED

#include <utils/Macro.h>
#include <vector>
#include <string>
#include <cstdlib>

namespace ce {

/// \brief A wrapper over std::vector with constant time erase access
template<typename T>
class Buffer{
public:
    typedef typename std::vector<T>::iterator       iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;

    Buffer()
    :   buffer_(),
        position_( 0u ),
        capacity_( 50000u ) {
        reserve( capacity_ );
    }
    ~Buffer() = default;

    void reserve( std::size_t capacity ) {
        buffer_.reserve( capacity );
        capacity_ = capacity;
    }

    void push_back( const T& t ) {
        ASSERT( position_ < capacity_, "Buffer::push_back: over capacity" );
        buffer_[ position_ ] = t;
        position_++;
    }

    void clear() {
        position_ = 0u;
    }

    /// \brief Erase an element at the given position.
    /// Returns an iterator to the element that immediately preceded the erased element. The iterator
    /// is the end-iterator if the erased element was the last element in the container.
    ///
    /// This method moves the last element into the position of the erased element, resulting in a
    /// constant time erase operation. This means that the order of the elements changes after each erase.
    iterator erase( std::size_t pos )  {
        ASSERT( pos >= 0u && pos < position_, "Buffer::erase: index out of range!" );
        buffer_[ pos ] = buffer_[ position_-1 ];
        position_--;
        return begin() + pos+1;
    }

    iterator erase( iterator pos ) {
        ASSERT( pos >= begin() && pos < end(), "Buffer::erase: iterator out of range!" );
        *pos = buffer_[ position_-1 ];
        position_--;
        return pos++;
    }

    T& operator[]( std::size_t pos ) {
		#ifdef DEBUG
        ASSERT( pos>=0u && pos <= position_, "Buffer::operator[]: index out of range!" );
		#endif
        return buffer_[pos];
    }

    const T& operator[]( std::size_t pos ) const {
        ASSERT( pos>=0u && pos <= position_, "Buffer::operator[]: index out of range!" );
        return buffer_[pos];
    }

    iterator begin() noexcept {
        return buffer_.begin();
    }

    const_iterator begin() const noexcept {
        return buffer_.begin();
    }

    iterator end() noexcept {
        return begin() + position_;
    }

    const_iterator end() const noexcept {
        return begin() + position_;
    }

    std::size_t size() const {
        return position_;
    }

private:
    std::vector<T>  buffer_;
    std::size_t     position_;
    std::size_t     capacity_;
};

}

#endif // BUFFER_H_INCLUDED
