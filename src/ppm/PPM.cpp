#include <ppm/PPM.h>
#include <fstream>
#include <cmath>

// implementation details
namespace {

float Clip( float s, float min, float max ) {
    return std::max( min, std::min( s, max ) );
}

float Cap( float s, float min, float max ) {
    return std::min( max, std::max( s, min ) );
}

char scalarToChar( float s, float min, float max ) {
    s = Clip( Cap( s, min, max ), min, max );
    return char ( 255.0f * ( s - min ) / ( max - min ) );
}

}

void ce::WritePPM( const std::vector<float>& scalars,
                  std::size_t nx, std::size_t ny,
                  const std::string& file,
                  ce::Mode mode ) {
    std::fstream fout( file, std::fstream::out | std::fstream::binary );
    fout << "P6\n";
    fout << nx << " " << ny << "\n255\n";
    for ( float f: scalars  ) {
        char color[3];
        if ( mode == ce::Mode::Density ) {
            color[0] = scalarToChar( f, 0.0f, 3.0f );
            color[1] = 30;
            color[2] = 30;
        } else if ( mode == ce::Mode::Discomfort ) {
            color[0] = 30;
            color[1] = 30;
            color[2] = scalarToChar( f, 0.0f, 1.0f );
        } else if ( mode == ce::Mode::Potential ) {
            color[0] = scalarToChar(f, 0.0f, 1.0f );
            color[1] = 30;
            color[2] = 30;
        }
        fout.write( color, 3 );
    }
}

void ce::WritePPM(
    const std::vector<DirectionalNode<float>>& scalars,
    std::size_t nx,
    std::size_t ny,
    const std::string& file,
    ce::Mode mode,
    ce::Direction direction
) {
    std::fstream fout ( file, std::fstream::out | std::fstream::binary );
    fout << "P6\n";
    fout << nx << " " << ny << "\n255\n";
    for ( auto& node: scalars ) {
        float value = 0.0f;
        switch ( direction ) {
            case ce::Direction::North: value = node.north; break;
            case ce::Direction::East: value = node.east; break;
            case ce::Direction::South: value = node.south; break;
            case ce::Direction::West: value = node.west; break;
        }
        char color[3];
        if ( mode == ce::Mode::Speed ) {
            color[0] = 30;
            color[1] = scalarToChar( value, 0.0f, 6.0f );
            color[2] = 30;
        } else if ( mode == ce::Mode::Cost ) {
            color[0] = 30;
            color[1] = 30;
            color[2] = scalarToChar( value, 0.0f, 2.0f );
        }
        fout.write( color, 3 );
    }
}
