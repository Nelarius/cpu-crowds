#ifndef FILE_H_INCLUDED
#define FILE_H_INCLUDED

#include <string>
#include <sys/stat.h>

namespace ce {

    /// \brief Check whether a file of the given name exists.
    inline bool file_exists( const std::string& file ) {
        struct stat buffer;
        return ( stat( file.c_str(), &buffer) == 0 );
    }

}

#endif // FILE_H_INCLUDED
