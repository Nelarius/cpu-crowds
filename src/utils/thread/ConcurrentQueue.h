#ifndef CONCURRENTQUEUE_H_INCLUDED
#define CONCURRENTQUEUE_H_INCLUDED

#include <queue>
#include <mutex>
#include <condition_variable>
namespace ce {

/// \brief A thread-safe wrapper around std::queue
template<typename T>
class ConcurrentQueue {
    public:
        ConcurrentQueue() = default;
        ~ConcurrentQueue() = default;

        bool empty() const {
            {
                std::unique_lock<std::mutex> lock( _mutex );
                return _queue.empty();
            }
        }

        void push( const T& t ) {
            {
                std::unique_lock<std::mutex> lock( _mutex );
                _queue.push( t );
            }
                _condition.notify_one();
        }

        void waitAndPop( T& t ) {
            {
                std::unique_lock<std::mutex> lock( _mutex );
                _condition.wait( lock, [this]() -> bool { return !_queue.empty(); } );
                t = std::move( _queue.front() );
                _queue.pop();
            }
        }

    private:
        std::queue<T>           _queue;
        mutable std::mutex      _mutex;
        std::condition_variable _condition;
};

}


#endif // CONCURRENTQUEUE_H_INCLUDED
