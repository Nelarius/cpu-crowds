CC=g++-4.9

CFLAGS = -pthread -std=c++11 -Wall -fexceptions -O2 -DDEBUG -I ./src

LDFLAGS = -lpthread 

OBJ = 	src/main.o \
	src/input/ObjectNode.o \
	src/input/ScriptParser.o \
	src/input/TokenStream.o \
	src/core/Application.o \
	src/core/NavigationGrid.o \
	src/utils/Random.o \
	src/utils/ThreadPool.o \
	src/ppm/PPM.o \
	
EXECUTABLE = crowds

all: $(EXECUTABLE)

%.oc : %.c
	gcc -std=c99 -c -o $@ $<

%.o : %.cpp
	$(CC) $(CFLAGS) -c -o $@ $<

$(EXECUTABLE): $(OBJ)
	$(CC) -o $(EXECUTABLE)  $(OBJ) $(LDFLAGS)

.PHONY: clean

clean:
	rm -r $(OBJ) $(EXECUTABLE)

clear:
	rm -r *.ppm
