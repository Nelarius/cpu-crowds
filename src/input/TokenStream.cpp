#include <input/TokenStream.h>
#include <input/SymbolicConstants.h>

#include <utils/Macro.h>

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <cctype>   //for isalpha, isdigit

using ce::TokenStream;
using ce::Token;

TokenStream::TokenStream( std::istream& in ) :  buffer_(),
                                                in_( in ) {
    //ctor
}

TokenStream::~TokenStream() {
    //dtor
}

Token TokenStream::get() {
    if ( !buffer_.empty() ) {
        Token t = buffer_.front();
        buffer_.pop();
        return t;
    }

    char ch;

    //check for end of file
    if ( in_.eof() ) {
        return Token( eof );
    }

    //prevent operator>>() from eating the newline
    if ( in_.peek() == '\n' ) {
        ch = in_.get();
    } else {
        in_ >> ch;
    }

    //check for comments
    checkComments_( &ch );

    switch( ch ) {
        case quit:
        case '\n':
        case '(':
        case ')':
        case '{':
        case '}':
        case '+':
        case '-':
        case '*':
        case '/':
        case '^':
        case '=':
            return Token( ch ); //let each operator represent itself
        //numeric literal can start with a point
        case '.':
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9': {
            //stick the leading digit or point of number back, and read it
            //from stream as a floating point literal
            in_.putback( ch );
            double val;
            in_ >> val;
            return Token( number, val );
        }

        default: {
            if ( isalpha( ch ) ) {
                std::string str;
                str += ch;
                //this defines a string:
                //it must begin with an alphanumeric character
                //after that, it can contain alphabetical characters,
                //numbers, or underscores
                while ( in_.get( ch ) &&
                       ( (isalpha( ch ) || isdigit( ch ) ) || ch == '_' ) ) {
                    str += ch;
                }
                in_.putback( ch );

                if ( str == declKey ) {
                    return Token( let );
                }
                return Token( name, str );
            }
            std::stringstream ss;
            ss << "error: bad token: '" << ch << "'" << std::endl;
            throw std::runtime_error( ss.str() );
        }
    }
}

void TokenStream::putback( const Token& token ) {
    buffer_.push( token );
}

void TokenStream::checkComments_( char* ch ) {
    while ( true ) {
        if ( *ch == comment ) {
            eatComment_();
        } else if ( *ch == openComment ) {
            eatLineComment_();
        } else {
            break;
        }
        //prevent operator>>() from eating the newline
        if ( in_.peek() == '\n' ) {
            *ch = in_.get();
        } else {
            in_ >> *ch;
        }
    }
}

void TokenStream::eatComment_() {
    char ch;
    while ( in_.get( ch ) && ch != '\n' );
}

void TokenStream::eatLineComment_() {
    char ch;
    while ( in_.get( ch ) && ch != closeComment );
}

