#include <input/ObjectNode.h>
#include <exception>
#include <cstdlib>  // for std::exit()
#include <iostream>

using ce::ObjectNode;

ObjectNode::ObjectNode( const std::string& name ) : _name( name ) {}

void ObjectNode::addNode( const std::string& name, const node_ptr& node ) {
    _nodes.insert( std::make_pair( name, node ) );
}

void ObjectNode::addNumber( const std::string& name, double value ) {
    _numbers.insert( std::make_pair( name, value) );
}

bool ObjectNode::isNumber( const std::string& name ) const {
    auto it = _numbers.find( name );
    if ( it != _numbers.end() ) {
        return true;
    }
    return false;
}

bool ObjectNode::isObjectNode( const std::string& name ) const {
    auto it = _nodes.find( name );
    if ( it != _nodes.end() ) {
        return true;
    }
    return false;
}

bool ObjectNode::contains( const std::string& name ) const {
    if ( isNumber( name ) || isObjectNode( name ) ) {
        return true;
    }
    return false;
}

double ObjectNode::operator()( const std::string& name ) const {
    try {
        return _numbers.at( name );
    } catch ( const std::exception& e ) {
        std::cout << "error: " << name << " not in " << _name << std::endl;
        std::exit(0);
    }
}

const ObjectNode& ObjectNode::operator[]( const std::string& name ) const {
    try {
        return *(_nodes.at( name ));
    } catch ( std::exception& e ) {
        std::cout << "error: " << name << " not in " << _name << std::endl;
        std::exit(0);
    }
}

const std::string& ObjectNode::name() const {
    return _name;
}

const number_map& ObjectNode::getNumbers() const {
    return _numbers;
}

const node_map& ObjectNode::getNodes() const {
    return _nodes;
}
