#include <core/NavigationGrid.h>
#include <utils/Macro.h>
#include <utils/Random.h>
#include <utils/Math.h>
#include <ppm/PPM.h>
#include <iostream>
#include <cmath>    // for floor
#include <thread>   // for thread::hardware_concurrency
#include <limits>
#include <algorithm>    // for min
#include <cmath>        //for abs, sqrt

using ce::NavigationGrid;
using ce::ObjectNode;

NavigationGrid::NavigationGrid( const ObjectNode& node )
        :   threadPool_(),
            dimx_( node("x") ),
            dimy_( node("y") ),
            nx_( node("nx") ),
            ny_( node("ny") ),
            dx_( dimx_ / nx_ ),
            dy_( dimy_ / ny_ ),
            timeStep_( node("time_step") ),
            agentCount_( node("agents") ),
            agentRadius_( node("agent_radius") ),
            discomfortRadius_( node("discomfort_radius") ),
            maxDensity_( node("rho_max") ),
            minDensity_( node("rho_min") ),
            maxSpeed_( node("f_max") ),
            pathWeight_( node("path_weight") ),
            timeWeight_( node("time_weight") ),
            discomfortWeight_( node("discomfort_weight") ),
            goalRadius_( node("goal_radius") ),
            innerLoopIterations_( node("inner_loop_iterations") ),
            convergenceEpsilon_( node("convergence_epsilon") ),
            tileSize_( node("tile_size") ),
            goalPoint_(),
            agents_(),
            discomfortSum_( nx_*ny_, 0.0f),
            densitySum_( nx_*ny_, 0.0f ),
            velocityDensitySum_( nx_*ny_, glm::vec2( 0.0f, 0.0f ) ),
            averageVelocity_( nx_*ny_, glm::vec2( 0.0f, 0.0f ) ),
            flowSpeed_( nx_*ny_, DirectionalNodef( 0.0f, 0.0f, 0.0f, 0.0f ) ),
            speed_( nx_*ny_, DirectionalNodef( 0.0f, 0.0f, 0.0f, 0.0f ) ),
            cost_( nx_*ny_, DirectionalNodef( 0.0f, 0.0f, 0.0f, 0.0f ) ),
            potential_( nx_*ny_, 0.0f ),
            oldPotential_( nx_*ny_, std::numeric_limits<float>::infinity() ) {
    if ( node.isNumber("threads") ) {
        threadPool_.initializeWithThreads( unsigned ( node("threads" ) ) );
    } else {
        threadPool_.initializeWithThreads( std::thread::hardware_concurrency() );
    }
    std::cout << "\nThis program is running with " << threadPool_.threads() << " threads\n";
}

NavigationGrid::~NavigationGrid() {
    //dtor
}

void NavigationGrid::initialize() {
    // generate agents on the left side of the grid
    for ( size_t i = 0; i < agentCount_; i++ ) {
        agents_.emplace_back(
            glm::vec2(
                ( 0.1f + 0.3f * ce::Randf() ) * dimx_,
                ( 0.05f + 0.9f * ce::Randf() ) * dimy_
            ),
            glm::vec2( 1.0f, 0.0f ),    //facing along the x-direction
            agentRadius_
        );
    }
    goalPoint_ = glm::vec2( 0.75f*dimx_, 0.5f*dimy_ );
    // initialize the discomfort field with a random bump somewhere
    splatDiscomfort_();

    //divide the grid into a set of tiles
    tileize_();
}

void NavigationGrid::update() {
    //clear previous densities
    for ( float& f: densitySum_ ) {
        f = 0.0f;
    }
    // cost calculation
    computeDensitySumVelocityDensitySum_();
    computeAverageVelocity_();
    computeFlowSpeed_();
    computeSpeed_();
    // potential calculation
    initializePotentialGrid_();

    ce::WritePPM(
        speed_,
        nx_,
        ny_,
        "speed.ppm",
        ce::Mode::Speed,
        ce::Direction::North
    );
    ce::WritePPM(
        cost_,
        nx_,
        ny_,
        "cost.ppm",
        ce::Mode::Cost,
        ce::Direction::North
    );
    ce::WritePPM(
        potential_,
        nx_,
        ny_,
        "potential.ppm",
        ce::Mode::Potential
    );
    // density sum is no longer needed, so we can clear the buffer
    /*threadPool_.schedule( [this]() -> void {
        for ( float& f: densitySum_ ) {
            f = 0.0f;
        }
    });*/
    computeCost_();
    solvePotential_();
    agentMover_();
    threadPool_.waitUntilAllFinished();

}

//////////////////////////////////////////////////////////////////
/////////////////// COST CALCULATION /////////////////////////////
//////////////////////////////////////////////////////////////////

/*
 * This method calculates the density sum and the average velocity sum
 * (essentially the the agent velocity multiplied by the agent's
 *  density at that point.)
 */
void NavigationGrid::computeDensitySumVelocityDensitySum_() {
    // PARALLEL CODE
    uint32_t chunk = nx_;
    uint32_t tasks = ny_;
    for ( uint32_t t = 0; t < tasks; t++ ) {
        uint32_t start = t * chunk;
        uint32_t end = (t + 1u ) * chunk;
        if ( t == tasks - 1 ) {
            end = densitySum_.size();
        }
        threadPool_.schedule( [start, end, t, this]() -> void {
            for ( size_t i = start; i < end; i++ ) {
                for ( ce::Agent& a: agents_ ) {
                    float r = glm::distance( getCoords_(i), a.position );
                    if ( r < a.radius ) {
                        float dens = a.getDensity( r );
                        densitySum_[i] += a.getDensity( r );
                        velocityDensitySum_[i] += dens * a.speed * a.direction;
                    }
                }   // for
            }  // for
        });
    }   // for
    threadPool_.waitUntilAllFinished();
}

/*
 * Computes the average velocity, normalized by the density sum at that point
 */
void NavigationGrid::computeAverageVelocity_() {
    // PARALLEL CODE
    uint32_t chunk = nx_;
    uint32_t tasks = ny_;
    for ( uint32_t t = 0; t < tasks; t++ ) {
        uint32_t start = t * chunk;
        uint32_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = averageVelocity_.size();
        }
        threadPool_.schedule( [ start, end, this ]() -> void {
            for ( uint32_t i = start; i < end; i++ ) {
                if ( densitySum_[i] != 0.0f ) {
                    averageVelocity_[i] = velocityDensitySum_[i] / densitySum_[i];
                } else {
                    averageVelocity_[i] = glm::vec2( 0.0f, 0.0f );
                }
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}

/*
 * Compute the flow speed. The flow speed is calculated by taking the
 * dot product of a direction and the neighboring cell's average
 * velocity in that direction.
 *
 * Each node has four flow speeds, one in each direction.
 */
void NavigationGrid::computeFlowSpeed_() {
    // PARALLEL CODE
    uint32_t chunk = nx_;
    uint32_t tasks = ny_;
    for ( uint32_t t = 0; t < tasks; t++ ) {
        uint32_t start = t * chunk;
        uint32_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = flowSpeed_.size();
        }
        threadPool_.schedule( [start, end, this ] () -> void {
            for ( uint32_t i = start; i < end; i++ ) {
                //stop moving after
                const float cutoff = 0.001;
                DirectionalNodef speed;
                speed.north     =   Cutoff(
                                        getVec2OrZero_(
                                            getRow_(i)+1u,
                                            getCol_(i),
                                            averageVelocity_
                                        ).y,
                                        cutoff
                                    );
                speed.east      =   Cutoff(
                                        getVec2OrZero_(
                                            getRow_(i),
                                            getCol_(i)+1u,
                                            averageVelocity_
                                        ).x,
                                        cutoff
                                    );
                speed.south     =   Cutoff(
                                        -getVec2OrZero_(
                                            getRow_(i)-1u,
                                            getCol_(i),
                                            averageVelocity_
                                        ).y,
                                        cutoff
                                    );
                speed.west      =   Cutoff(
                                        -getVec2OrZero_(
                                            getRow_(i),
                                            getCol_(i)-1u,
                                            averageVelocity_
                                        ).x,
                                        cutoff
                                    );

                flowSpeed_[i]   = speed;
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}

void NavigationGrid::computeSpeed_() {
    // PARALLEL CODE
    std::uint32_t chunk = nx_;
    std::uint32_t tasks = ny_;
    for ( std::uint32_t t = 0; t < tasks; t++ ) {
        std::uint32_t start = t * chunk;
        std::uint32_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = speed_.size();
        }
        threadPool_.schedule( [ start, end, this ]() -> void {
            for ( std::uint32_t i = start; i < end; i++ ) {
                /*
                 * THIS SEGMENT OF CODE COULD REALLY USE REFLECTION OR SOMETHING
                 */
                DirectionalNodef speed;
                const DirectionalNodef flowSpeed = flowSpeed_[i];

                // NORTH
                const float densityN = getScalarOrInfinity_(
                    getRow_(i) + 1u,
                    getCol_(i),
                    densitySum_
                );
                // handle off-grid direction
                if ( densityN == std::numeric_limits<float>::infinity() ) {
                    speed.north = 0.0f;
                } else if ( densityN > maxDensity_ ) {
                    speed.north = flowSpeed.north;
                } else if ( densityN <= maxDensity_ && densityN > minDensity_ ) {
                    speed.north = maxSpeed_;
                    speed.north += ( densityN - minDensity_ ) / ( maxDensity_ - minDensity_ );
                    speed.north *= (flowSpeed.north - maxSpeed_ );
                } else if ( densityN <= minDensity_ ) {
                    speed.north = maxSpeed_;
                } else {
                    ASSERT( false, "NavigationGrid::computeSpeed_> Negative density in north direction!" );
                }

                // EAST
                const float densityE = getScalarOrInfinity_(
                    getRow_(i),
                    getCol_(i) + 1u,
                    densitySum_
                );
                // handle off-grid direction
                if ( densityE == std::numeric_limits<float>::infinity() ) {
                    speed.east = 0.0f;
                } else if ( densityE > maxDensity_ ) {
                    speed.east = flowSpeed.east;
                } else if ( densityE <= maxDensity_ && densityE > minDensity_ ) {
                    speed.east = maxSpeed_;
                    speed.east += ( densityE - minDensity_ ) / ( maxDensity_ - minDensity_ );
                    speed.east *= ( flowSpeed.east - maxSpeed_ );
                } else if ( densityE <= minDensity_ ) {
                    speed.east = maxSpeed_;
                } else {
                    ASSERT( false, "NavigationGrid::computeSpeed_> Negative density in eastern direction!" );
                }

                // SOUTH
                const float densityS = getScalarOrInfinity_(
                    getRow_(i) - 1u,
                    getCol_(i),
                    densitySum_
                );
                //handle off grid direction
                if ( densityS == std::numeric_limits<float>::infinity() ) {
                    speed.south = 0.0f;
                } else if ( densityS > maxDensity_ ) {
                    speed.south = flowSpeed.south;
                } else if ( densityS <= maxDensity_ && densityS > minDensity_ ) {
                    speed.south = maxSpeed_;
                    speed.south += ( densityS - minDensity_ ) / ( maxDensity_ - minDensity_ );
                    speed.south *= flowSpeed.south - maxSpeed_;
                } else if ( densityS <= minDensity_ ) {
                    speed.south = maxSpeed_;
                } else {
                    ASSERT( false, "NavigatioNGrid::computeSpeed_> Negative density in southern direction!" );
                }

                // WEST
                const float densityW = getScalarOrInfinity_(
                    getRow_(i),
                    getCol_(i) - 1u,
                    densitySum_
                );
                // handle off-grid direction
                if ( densityW == std::numeric_limits<float>::infinity() ) {
                    speed.west = 0.0f;
                } else if ( densityW > maxDensity_ ) {
                    speed.west = flowSpeed.west;
                } else if ( densityW <= maxDensity_ && densityW > minDensity_ ) {
                    speed.west = maxSpeed_;
                    speed.west += ( densityW - minDensity_ ) / ( maxDensity_ - minDensity_ );
                    speed.west *= flowSpeed.west - maxSpeed_;
                } else if ( densityW <= minDensity_ ) {
                    speed.west = maxSpeed_;
                } else {
                    ASSERT( false, "NavigationGrid::computeSpeed_> Negative density in western direction!" );
                }

                speed_[i] = speed;
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}

void NavigationGrid::computeCost_() {
    // PARALLEL CODE
    std::uint32_t chunk = nx_;
    std::uint32_t tasks = ny_;
    for ( std::uint32_t t = 0; t < tasks; t ++) {
        std::uint32_t start = t * chunk;
        std::uint32_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = cost_.size();
        }
        threadPool_.schedule( [start, end, this ]() -> void {
            for ( std::uint32_t i = start; i < end; i++ ) {
                // get the discomfort value (g) in each direction
                const float gN =  getScalarOrInfinity_(
                    getRow_(i) + 1u,
                    getCol_(i),
                    discomfortSum_
                );
                const float gE = getScalarOrInfinity_(
                    getRow_(i),
                    getCol_(i) + 1u,
                    discomfortSum_
                );
                const float gS = getScalarOrInfinity_(
                    getRow_(i) - 1u,
                    getCol_(i),
                    discomfortSum_
                );
                const float gW = getScalarOrInfinity_(
                    getRow_(i),
                    getCol_(i) - 1u,
                    discomfortSum_
                );
                DirectionalNodef cost;
                DirectionalNodef speed = speed_[i];

                cost.north = pathWeight_*speed.north + timeWeight_ + discomfortWeight_ * gN;
                cost.north /= speed.north;

                cost.east = pathWeight_*speed.east + timeWeight_ + discomfortWeight_ * gE;
                cost.east /= speed.east;

                cost.south = pathWeight_*speed.south + timeWeight_ + discomfortWeight_ * gS;
                cost.south /= speed.south;

                cost.west = pathWeight_*speed.west + timeWeight_ + discomfortWeight_ * gW;
                cost.west /= speed.west;

                cost_[i] = cost;
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}

void NavigationGrid::splatDiscomfort_() {
    //the discomfort circle is located at the center of the grid
    glm::vec2 r0( 0.5f * dimx_, 0.5f * dimy_ );
    auto getDiscomfort = [this]( float r ) -> float {
        if ( r > discomfortRadius_ ) {
            return 0.0f;
        }
        return 1.0f - r / discomfortRadius_;
    };
    for ( size_t i = 0; i < discomfortSum_.size(); i++ ) {
        discomfortSum_[i] = getDiscomfort( glm::distance( getCoords_(i), r0 ) );
    }
}

std::shared_ptr<ObjectNode> NavigationGrid::nodeRepresentation() const {
    std::shared_ptr<ObjectNode> node( new ObjectNode( "grid" ) );
    node->addNumber( "x", dimx_ );
    node->addNumber( "y", dimy_ );
    node->addNumber( "xn", nx_ );
    node->addNumber( "yn", ny_ );
    node->addNumber( "agents", agentCount_ );
    node->addNumber( "agent_radius", agentRadius_ );
    node->addNumber( "f_max", maxSpeed_ );
    node->addNumber( "rho_min", minDensity_ );
    node->addNumber( "rho_max", maxDensity_ );
    node->addNumber( "discomfort_radius", discomfortRadius_ );
    node->addNumber( "path_weight", pathWeight_ );
    node->addNumber( "time_weight", timeWeight_ );
    node->addNumber( "discomfort_weight", discomfortWeight_ );
    node->addNumber( "finner_loop_iterations", innerLoopIterations_ );

    /*THIS IS A WORK IN PROGRESS*/

    return node;
}

//////////////////////////////////////////////////////////////////
////////////////////// MISCELLANEOUS /////////////////////////////
//////////////////////////////////////////////////////////////////

const std::vector<float>& NavigationGrid::getDensitySum() const {
    return densitySum_;
}

const std::vector<float>& NavigationGrid::getDiscomfortSum() const {
    return discomfortSum_;
}

size_t NavigationGrid::getXNodeCount() const {
    return nx_;
}

size_t NavigationGrid::getYNodeCount() const {
    return ny_;
}

glm::vec2 NavigationGrid::getVec2OrZero_(
                                std::size_t row,
                                std::size_t col,
                                const std::vector<glm::vec2>& v ) const {
    if ( row >= ny_ ) {
        return glm::vec2( 0.0f, 0.0f );
    }
    if ( col >= nx_ ) {
        return glm::vec2( 0.0f, 0.0f );
    }
    return v[ getCell_( row, col ) ];
}

float NavigationGrid::getScalarOrInfinity_(
                                    std::size_t row,
                                    std::size_t col,
                                    const std::vector<float>& v ) const {
    if ( row >= ny_ ) {
        return std::numeric_limits<float>::infinity();
    }
    if ( col >= nx_ ) {
        return std::numeric_limits<float>::infinity();
    }
    return v[ getCell_( row, col ) ];
}

//////////////////////////////////////////////////////////////////
////////////////// POTENTIAL CALCULATION /////////////////////////
//////////////////////////////////////////////////////////////////

float NavigationGrid::solveQuadratic_( float px, float py, float cx, float cy ) {
    float solution;

    float potentialMin = std::min( px, py );
    float difference = px - py;
    float absoluteDifference = fabs( px - py );
    float costSquared = ( cx + cy ) * ( cx + cy );

    if ( absoluteDifference > ( cx + cy ) ) {
        // solve 1D case
        solution = potentialMin + cx + cy;
    } else {
        // else, solve the 2D case
        solution = 0.5f * ( px + py + sqrt( 2.0f*costSquared - difference*difference ) );
    }

    return solution;
}

void NavigationGrid::initializePotentialGrid_() {
    // PARALLEL CODE
    std::uint32_t chunk = nx_;
    std::uint32_t tasks = ny_;
    for ( std::uint32_t t = 0; t < tasks; t++ ) {
        std::uint32_t start = t * chunk;
        std::uint32_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = cost_.size();
        }
        threadPool_.schedule( [start, end, this]() -> void {
            for ( std::uint32_t i = start; i < end; i++ ) {
                // if we're withing the goal circle, set the potential to zero
                if ( glm::length( getCoords_(i) - goalPoint_ ) < goalRadius_ ) {
                    potential_[i] = 0.0f;
                } else {
                    potential_[i] = std::numeric_limits<float>::infinity();
                }
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}

void NavigationGrid::tileize_() {
    std::size_t xchunk = nx_ / tileSize_;
    std::size_t ychunk = ny_ / tileSize_;
    for ( std::size_t i = 0; i < ychunk; i++ ) {
        for ( std::size_t j = 0; j < xchunk; j++ ) {
            // this is a hidden pitfall!
            // the final column & row might take up to twice as much time as the previous rows
            // not good load-balancing
            if ( i == ychunk - 1u && j == xchunk - 1u ) {
                tileList_.emplace_back( j*tileSize_, nx_, i*tileSize_, ny_ );
            } else if ( i == ychunk - 1u ) {
                tileList_.emplace_back( j*tileSize_, (j+1u) * tileSize_, i*tileSize_, ny_ );
            } else if ( j == xchunk - 1u ) {
                tileList_.emplace_back( j*tileSize_, nx_, i*tileSize_, (i+1u) * tileSize_ );
            } else {
                tileList_.emplace_back( j*tileSize_, (j+1u)*tileSize_, i*tileSize_, (i+1u) * tileSize_ );
            }
        }   // for j
    }   // for i
}

void NavigationGrid::updateCell_( std::size_t i, std::size_t j ) {
	// our index
	std::size_t index = getCell_( i, j );
	// select px
	
	// !!!!!!!!!!!!!!!!!!!!!
	float westVal = getScalarOrInfinity_( i, j-1u, potential_ ) + cost_[index].west;
	float eastVal = getScalarOrInfinity_( i, j+1u, potential_ ) + cost_[index].east;
	float px = westVal <= eastVal ? potential_[getCell_(i, j-1u)] : potential_[getCell_(i, j+1u)];
	float cx = westVal <= eastVal ? cost_[index].west : cost_[index].east;
	// select py
	float northVal = getScalarOrInfinity_( i+1u, j, potential_ ) + cost_[index].north;
	float southVal = getScalarOrInfinity_( i-1u, j, potential_ ) + cost_[index].south;
	float py = southVal <= northVal ? potential_[getCell_(i-1u, j)] : potential_[getCell_(i+1u, j)];
	float cy = southVal <= northVal ? cost_[index].south : cost_[index].north;
	// store the old value
	oldPotential_[index] =  potential_[index];
	// solve the quadratic equation using these cells
	potential_[index] = solveQuadratic_( px, py, cx, cy );
}

void NavigationGrid::innerLoop_( Tile& t ) {
    // this depends on innerLoopIterations_ and convergenceEpsilon_
    for ( std::size_t n = 0; n < innerLoopIterations_; n++ ) {
		// for each cell within the tile
        for ( std::size_t j = t.colStart; j < t.colEnd; j++ ) {
            for ( std::size_t i = t.rowStart; i < t.rowEnd; i++ ) {
                updateCell_( i, j );
            } //for each row
        } //for each column
    } // for each tile
}

/*
 * sets tile.isConverged = false on those tiles which contain
 * even one unconverged grid point
 * */
void NavigationGrid::convergenceReduction_( Tile& t ) {
    t.isConverged = true;
    bool flag = false;
    for ( std::size_t j = t.colStart; j < t.colEnd; j++ ) {
        for ( std::size_t i = t.rowStart; i < t.rowEnd; i++ ) {
            // if a single grid point is unconverged, then the entire tile is unconverged
            if ( fabs( potential_[getCell_( i, j )] - oldPotential_[getCell_( i, j )] ) > convergenceEpsilon_ ) {
                t.isConverged = false;
                flag = true;
                break;
            }
        }
        if ( flag ) {
            break;
        }
    }
}

void NavigationGrid::solvePotential_() {
    // step 1:
    // adds tiles in goal areas to the active list
    markGoalTiles_();

    bool firstIteration = true;
    // until all tiles are converged
    do {
        // 1 task for each tile
        // the active list index is thus the task index
        std::size_t tasks = activeList_.size();
        /*for ( std::size_t t = 0; t < tasks; t++ ) {
            // update each tile in a thread
            // innerLoop_ contains steps 2a
            threadPool_.schedule( [t, this]() -> void { 
				
				#ifdef DEBUG
				if ( t > activeList_.size() ) {
					std::cout << "t: " << t << ", activeList_: " << activeList_.size() << std::endl;
				}
				ASSERT( t < activeList_.size(), "error: t running amok in thread" );
				#endif
                innerLoop_( activeList_[t] );
            });
        }
        threadPool_.waitUntilAllFinished(); // SYNCHRONIZE*/
		for ( auto& tile: activeList_ ) {
			innerLoop_( tile );
		}
        // convergence reduction
        // update tile convergence
		/*tasks = activeList_.size();
        for ( std::size_t t = 0; t < tasks; t++ ) {
            threadPool_.schedule( [t, this]() -> void {
				#ifdef DEBUG
				if ( t > activeList_.size() ) {
					std::cout << "t: " << t << ", activeList_: " << activeList_.size() << std::endl;
				}
				ASSERT( t < activeList_.size(), "error: t running amok in thread" );
				#endif
                convergenceReduction_( activeList_[t] );
            });
        }
        threadPool_.waitUntilAllFinished(); // SYNCHRONIZE*/
		for ( auto& tile: activeList_ ) {
			convergenceReduction_( tile );
		}
        
        // add all non-converged tiles to the active list
        if ( firstIteration ) {
            firstIteration = false;
            activeList_.clear();
            for ( auto& t: tileList_ ) {
                if ( !t.isConverged ) {
                    activeList_.push_back( t );
                }
            }
        }
        // this removes a tile if converged
        // calls update1Connected on the removed tile
        removeConvergedTiles_();
		std::cout << "activeList_.size(): " << activeList_.size() << std::endl;
    } while( activeList_.size() > 0u );
}

/*
 * Adds tiles containing grid points within goal cells to
 * the active tile list
 */
void NavigationGrid::markGoalTiles_() {
    // step 1:
    // mark the goal tiles
    for ( auto& t: tileList_ ) {
        bool flag = false;
        for ( std::size_t j = t.colStart; j < t.colEnd; j++ ) {
            for ( std::size_t i = t.rowStart; i < t.rowEnd; i++ ) {
                if ( glm::length( getCoords_(getCell_( i, j )) - goalPoint_ ) < goalRadius_ ) {
                    activeList_.push_back( t );
                    flag = true;
                    break;
                }
            } // for rows
            if ( flag ) {
                break;
            }
        } // for columns
    } // for each tile
}


/*
 * Removes all converged tiles.
 * Calls update1Connected to propagate the converged solution
 * to neighboring tiles
 */
void NavigationGrid::removeConvergedTiles_() {
    for ( std::size_t i = activeList_.size()-1u; i != 0u; i-- ) {
        if ( activeList_[i].isConverged ) {
			#ifdef DEBUG
			if ( i > activeList_.size() ) {
				std::cout << "i: " << i << ", activeList_: " << activeList_.size() << std::endl;
			}
			ASSERT( i < activeList_.size(), "error: t running amok in thread" );
			#endif
            update1Connected( activeList_[i] );
            activeList_.erase( i );
        }
    }
}

void NavigationGrid::update1Connected( Tile& t ) {
    if ( t.colStart != 0u ) {
        for ( std::size_t i = t.rowStart; i < t.rowEnd; i++ ) {
			updateCell_( i, t.colStart - 1u );
		}
    }
    if ( t.colEnd != nx_-1u ) {
        for ( std::size_t i = t.rowStart; i < t.rowEnd; i++ ) {
			updateCell_( i, t.colStart + 1u );
		}
    }
    if ( t.rowStart != 0u ) {
        for ( std::size_t j = t.colStart; j < t.colEnd; j++ ) {
			updateCell_( t.rowStart - 1u, j );
		}
    }
    if ( t.rowEnd != ny_-1u ) {
        for ( std::size_t j = t.colStart; j < t.colEnd; j++ ) {
			updateCell_( t.rowEnd + 1u, j );
		}
    }
}


//////////////////////////////////////////////////////////////////
////////////////////// AGENT UPDATER /////////////////////////////
//////////////////////////////////////////////////////////////////

void NavigationGrid::agentMover_() {
    // PARALLEL CODE
    std::size_t chunk = agents_.size() / threadPool_.threads();
    std::size_t tasks = threadPool_.threads();
    for( std::size_t t = 0; t < tasks; t++ ) {
        std::size_t start = t * chunk;
        std::size_t end = (t + 1u) * chunk;
        if ( t == tasks - 1u ) {
            end = agents_.size();
        }
        threadPool_.schedule( [start, end, this]() -> void {
            for ( std::size_t i = start; i < end; i++ ) {
                Agent& a = agents_[i];
                a.position += timeStep_ * a.speed * a.direction;
            }
        });
    }
    threadPool_.waitUntilAllFinished();
}


