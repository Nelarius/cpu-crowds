#ifndef MACRO_H_INCLUDED
#define MACRO_H_INCLUDED

#include <cstdlib>
#include <iostream>

//defined as a do-while so that a semicolon can be used
#define ASSERT(condition, message) \
do { \
    if (! (condition)) { \
        std::cerr << "Assertion '" #condition "' failed in " << __FILE__ \
                  << " line " << __LINE__ << ": " << message << std::endl; \
        std::exit(EXIT_FAILURE); \
    } \
} while (false) \


#endif // MACRO_H_INCLUDED
