#ifndef OBJECTNODE_H_INCLUDED
#define OBJECTNODE_H_INCLUDED

#include <memory>
#include <map>
#include <string>

namespace ce {
    class ObjectNode;
}

typedef std::map<const std::string, double>                 number_map;
typedef std::shared_ptr<ce::ObjectNode>                     node_ptr;
typedef std::map<const std::string, node_ptr>               node_map;
typedef std::map<const std::string, node_ptr>::iterator     node_iterator;

namespace ce {

/// \brief A node in an object hierarchy.
/// The ObjectNode can store numbers, or other ObjectNodes.
class ObjectNode {
public:
    explicit ObjectNode( const std::string& name );
    ~ObjectNode() = default;

    /// \brief Get the value of a number.
    double operator()( const std::string& name ) const;

    /// \brief Get the reference of a node.
    const ObjectNode& operator[]( const std::string& name ) const;

    void addNode( const std::string&, const node_ptr& node );
    void addNumber( const std::string&, double value );

    /// \brief Check to see if the name is a number
    bool isNumber( const std::string& name ) const;

    /// \brief Check to see if the name is an ObjectNode
    bool isObjectNode( const std::string& name ) const;

    /// \brief Check whether the node contains the name.
    bool contains( const std::string& name ) const;

    /// \brief Get the name of this node.
    const std::string& name() const;

    const number_map&   getNumbers() const;
    const node_map&     getNodes() const;

private:
    const std::string _name;
    number_map      _numbers;
    node_map        _nodes;

};

}   //namespace ce


#endif // OBJECTNODE_H_INCLUDED
