#ifndef DIRECTIONALNODE_H_INCLUDED
#define DIRECTIONALNODE_H_INCLUDED

namespace ce {

template <typename T>
struct DirectionalNode {
    DirectionalNode() : north(), south(), east(), west()    {}
    DirectionalNode( T n, T s, T e, T w)  : north(n), south(s), east(e), west(w) {}

    T north;
    T south;
    T east;
    T west;
};

}

#endif // DIRECTIONALNODE_H_INCLUDED
