#include <input/ScriptParser.h>
#include <input/SymbolicConstants.h>
#include <utils/Macro.h>

#include <sstream>
#include <cmath>    //for pow()
#include <cstdlib>  //for exit()
#include <iostream>
#include <stdexcept>

using ce::ScriptParser;

ScriptParser::ScriptParser( std::istream& in ) 
    :   _tstream( in ),
        _in( in ) {
    //ctor
}

void ScriptParser::parse() {
    ASSERT( _in.good(), "error: input stream no good" );
    while ( _in.good() ) {
        try {
            Token t = _tstream.get();
            //discard all newlines
            while ( t.kind == '\n' ) {
                t = _tstream.get();
            }
            //handle end of file:
            if ( t.kind == eof ) {
                break;
            }
            _tstream.putback( t );
            _statement();
        } catch ( const std::exception& e ) {
            std::cout << e.what() << std::endl;
            std::exit( 0 );
        }
    }
}

double ScriptParser::operator()( const std::string& name ) const {
    try {
        return _variables.at( name );
    } catch ( const std::exception& e ) {
        std::cout << "error: " << name << " doesn't exist.\n";
        std::exit(0);
    }
}

const ce::ObjectNode& ScriptParser::operator[]( const std::string& name ) const {
    try {
        return *(_nodes.at(name) );
    } catch ( const std::exception& e ) {
        std::cout << "error: " << name << " doesn't exist.\n";
        std::exit(0);
    }
}

void ScriptParser::declare( const std::string& name, double value ) {
    _variables.insert( std::make_pair( name, value ) );
}

const variable_map& ScriptParser::getVariables() const {
    return _variables;
}

const node_map& ScriptParser::getNodes() const {
    return _nodes;
}

bool ScriptParser::_isDeclared( const std::string& str ) const {
    if ( _isNumber( str ) || _isObject( str ) ) {
        return true;
    }
    return false;
}

bool ScriptParser::_isNumber( const std::string& str ) const {
    map_iterator it = _variables.find( str );
    if ( it != _variables.end() ) {
        return true;
    }
    return false;
}

bool ScriptParser::_isObject( const std::string& str ) const {
    auto it = _nodes.find( str );
    if ( it != _nodes.end() ) {
        return true;
    }
    return false;
}

void ScriptParser::_assignName( const std::string& name, double val ) {
    if ( !_isDeclared( name ) ) {
        std::stringstream ss;
        ss << "error: " << name << " not defined" << std::endl;
        throw std::runtime_error( ss.str() );
    }
    _variables[ name ] = val;
}

void ScriptParser::_defineName( const std::string& name, double val ) {
    if ( _isDeclared( name ) ) {
        std::stringstream ss;
        ss << "error: " << name << " already declared" << std::endl;
        throw std::runtime_error( ss.str() );
    }
    _variables.insert( std::make_pair( name, val ) );
}

double ScriptParser::_statement() {
    Token t = _tstream.get();
    switch( t.kind ) {
        case let:
            return _declaration();
        case name: {
            Token t2 = _tstream.get();
            _tstream.putback( t );
            _tstream.putback( t2 );
            if ( t2.kind == '=' ) {
                return _assignment();
            } else {
                return _expression();
            }
        }
        default:
            _tstream.putback( t );
            return _expression();
    }
}

double ScriptParser::_declaration() {
    //assume we have seen let
    //then we handle
    //      name = expression
    Token t = _tstream.get();
    if ( t.kind != name ) {
        throw std::runtime_error( "error: expected name in declaration" );
    }

    std::string varName = t.name;
    //check whether a variable of this name already exists
    if ( _isDeclared( varName ) ) {
        std::stringstream ss;
        ss << "error: " << varName << " already declared" << std::endl;
        throw std::runtime_error( ss.str() );
    }

    Token t2 = _tstream.get();

    if ( t2.kind != '=' ) {
        std::stringstream ss;
        ss << "error: '=' missing in declaration of " << varName << std::endl;
        throw std::runtime_error( ss.str() );
    }

    // branch out into expression or object traversal:
    Token t3 = _tstream.get();
    if ( t3.kind == '{' ) {
        _defineObject( varName, _objectTraversal( varName ) );
        // I need to get rid of returns
        return 0.0;
    } else {
        _tstream.putback( t3 );
    }

    double val = _expression();
    _defineName( varName, val );
    return val;
}

double ScriptParser::_expression() {
    double left = _term();
    Token t = _tstream.get();

    while ( true ) {
        switch( t.kind ) {
            case '+':
                left += _term();
                t = _tstream.get();
                break;
            case '-':
                left -= _term();
                t = _tstream.get();
                break;
            default:
                _tstream.putback( t );
                return left;
        }
    }
}

double ScriptParser::_assignment() {
    Token t = _tstream.get();

    std::string varName = t.name;
    Token t2 = _tstream.get();

    if ( t2.kind != '=' ) {
        std::stringstream ss;
        ss << "error: '=' missing in declaration of " << varName  << std::endl;
        throw std::runtime_error( ss.str() );
    }

    // branch out into expression or object traversal:
    Token t3 = _tstream.get();
    if ( t3.kind == '{' ) {
        //check type
        if ( _isNumber( varName ) ) {
            std::stringstream ss;
            ss << "error: trying to assign an object to " << varName;
            ss << ", which is a number!" << std::endl;
            throw std::runtime_error( ss.str() );
        }
        _assignObject( varName, _objectTraversal( varName ) );
        // I need to get rid of returns
        return 0.0;
    } else {
        _tstream.putback( t3 );
    }

    //check type
    if ( _isObject( varName ) ) {
        std::stringstream ss;
        ss << "error: trying to assign a number to " << varName;
        ss << ", which is an object!" << std::endl;
        throw std::runtime_error( ss.str() );
    }
    double val = _expression();
    _assignName( varName, val );
    return val;
}

double ScriptParser::_term() {
    double left = _primary();
    Token t = _tstream.get();

    while ( true ) {
        switch( t.kind ) {
            case '*':
                left *= _primary();
                t = _tstream.get();
                break;
            case '/': {
                double val = _primary();
                if ( val == 0.0 ) {
                    throw std::runtime_error( "error: divide by zero" );
                }
                left /= val;
                t = _tstream.get();
                break;
            }
            case '^':
                left = pow( left, _primary() );
                t = _tstream.get();
            default:
                _tstream.putback( t );
                return left;
        }
    }
}

double ScriptParser::_primary() {
    Token t = _tstream.get();

    switch( t.kind ) {
        case '(': {
            double val = _expression();
            t = _tstream.get();
            if ( t.kind != ')' ) {
                throw std::runtime_error( "error: ')' expected" );
            }
            return val;
        }
        case number:   return t.value;
        case name: {
            auto it = _variables.find( t.name );
            if ( it != _variables.end() ) {
                return it->second;
            }
            std::stringstream ss;
            ss << "error: name \"" << t.name << "\" not declared";
            throw std::runtime_error( ss.str() );
        }
        case '+':   return _primary();
        case '-':   return -_primary();
        default:    throw std::runtime_error( "error: primary expected" );

    }
}

node_ptr ScriptParser::_objectTraversal( const std::string& parentName ) {
    auto node = node_ptr( new ObjectNode( parentName ) );

    while ( true ) {
        Token t = _tstream.get();

        //handle possible newlines
        while ( t.kind == '\n' ) {
            t = _tstream.get();
        }

        if ( t.kind == '}' ) {
            break;
        }
        // must be a name, or there is a syntax error
        if ( t.kind == ce::name ) {
            Token t2 = _tstream.get();
            const std::string memberName( t.name );
            // must be assignment or else, there is a syntax error
            if ( t2.kind == '=' ) {
                //branch out to number assignment or object assignment
                Token t3 = _tstream.get();
                if ( t3.kind == '{' ) {
                    node->addNode( memberName, _objectTraversal( memberName ) );
                } else {
                    _tstream.putback( t3 );
                    node->addNumber( memberName, _expression() );
                }
            } else {
                std::stringstream ss;
                ss << "error: expected assignment operator after " << memberName;
                ss << " in object " << parentName << std::endl;
                throw std::runtime_error( ss.str() );
            }
        } else {
            std::stringstream ss;
            ss << "error: expected name in object " << parentName << std::endl;
        }
    }
    return node;
}

void ScriptParser::_defineObject( const std::string& name, node_ptr node ) {
    _nodes.insert( std::make_pair( name, node ) );
}

void ScriptParser::_assignObject( const std::string& name, node_ptr node ) {
    if ( !_isObject( name ) ) {
        std::stringstream ss;
        ss << "error: " << name << " not defined" << std::endl;
        throw std::runtime_error( ss.str() );
    }
    _nodes[name] = node;
}
