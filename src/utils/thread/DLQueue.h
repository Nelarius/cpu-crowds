#ifndef DLQUEUE_H_INCLUDED
#define DLQUEUE_H_INCLUDED

#include <utils/Uncopyable.h>
#include <mutex>
#include <memory>
#include <atomic>

namespace ce {

/// \brief A thread-safe queue.
///
/// This queue can be pushed and popped from at the same time from different threads.
/// It works because the head will always be a dummy node.
/// We do this, because we cannot reference the head and the tail node in the same
/// method. Doing so would not allow this data structure to be pushed and popped from at
/// the same time, since the head and tail would be locked under the same mutex.
template<typename T>
// TODO: make it copyable
class DLQueue : private Uncopyable {

    public:
        DLQueue();
        ~DLQueue() = default;

        /// \brief Poll whether the queue is empty or not.
        bool empty() const;
        /// \brief Push an element onto the end of the queue.
        void     push( const T& element );
        /// \brief Pop an element from the front of the queue.
        /// \param t The variable which captures the popped element.
        /// \return true, if an element was popped, false otherwise.
        bool     pop( T& t );

        /// \brief Get the number of elements in the container.
        size_t   size() const;

    private:

        struct Node {
            T element;
            std::shared_ptr<Node> next;

            Node() = default;
            Node(
                const T& t,
                std::shared_ptr<Node> n = nullptr
            ) : element( t ),
                next( n ) {}
        };

        //FIELDS
        std::atomic<std::size_t> _size;
        std::shared_ptr<Node>    _head;
        std::shared_ptr<Node>    _tail;
        std::mutex               _hmutex;
        std::mutex               _tmutex;

};

}   // namespace ce

#include <utils/thread/DLQueue.tpp>

#endif // DLQUEUE_H_INCLUDED
