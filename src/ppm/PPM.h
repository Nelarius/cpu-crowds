#ifndef PPM_H_INCLUDED
#define PPM_H_INCLUDED

#include <core/DirectionalNode.h>
#include <vector>
#include <string>
#include <cstdlib>

namespace ce {

enum Mode {
    Density = 1,
    Discomfort,
    Speed,
    Cost,
    Potential
};

enum Direction {
    North = 1,
    East,
    South,
    West
};

void WritePPM(
    const std::vector<float>&,
    std::size_t,
    std::size_t,
    const std::string&,
    Mode = Density
);

void WritePPM(
    const std::vector<DirectionalNode<float>>&,
    std::size_t,
    std::size_t,
    const std::string&,
    Mode = Speed,
    Direction = North
);
}

#endif // PPM_H_INCLUDED
