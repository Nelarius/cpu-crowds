#ifndef UNCOPYABLE_H
#define UNCOPYABLE_H

namespace ce {

/// \brief A base class which hides its copy and assignment constructors.
/// To use, simply do
/// \code
/// class WhatWeWantToProtect : private Uncopyable {
///     public:
///     private:
/// };
/// \endcode
class Uncopyable {
    protected:
        Uncopyable() = default;
        Uncopyable( const Uncopyable& ) = delete;
        Uncopyable& operator=( const Uncopyable& ) = delete;
        virtual ~Uncopyable() = default;
};

}

#endif // UNCOPYABLE_H
