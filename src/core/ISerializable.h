#ifndef ISERIALIZABLE_H_INCLUDED
#define ISERIALIZABLE_H_INCLUDED

#include <input/ObjectNode.h>

namespace ce {

/// \brief A serializable base class.
/// A serializable class is responsible for interpreting
/// and constructing its own representation.
class ISerializable {
    public:
        ISerializable() = default;
        ISerializable( const ObjectNode& node ) {};
        virtual node_ptr nodeRepresentation() const = 0;
};

}   // namespace ce

#endif // ISERIALIZABLE_H_INCLUDED

