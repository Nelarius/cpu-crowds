#ifndef TOKEN_H_INCLUDED
#define TOKEN_H_INCLUDED

#include <string>

namespace ce {

struct Token {
    char kind;
    double value;
    std::string name;

    Token( char ch = 'w' ) :    kind( ch ),
                                value( 0.0 ),
                                name( "" )  {}

    Token( char ch, double val ) :  kind( ch ),
                                    value( val ),
                                    name( "" )  {}

    Token( char ch, const std::string& str ) :  kind( ch ),
                                                value( 0.0 ),
                                                name( str ) {}
};


}

#endif // TOKEN_H_INCLUDED
