#ifndef APPLICATION_H
#define APPLICATION_H

#include <utils/Uncopyable.h>
#include <input/ScriptParser.h>
#include <core/NavigationGrid.h>
#include <cstdlib>

namespace ce {

class Application: private Uncopyable {
    public:
        Application( const ObjectNode& );
        ~Application() = default;

        void run();

    private:
        void initialize_();

        NavigationGrid  grid_;
        float           dt_;
        std::uint32_t   iterations_;
};

}   //namespace ce

#endif // APPLICATION_H
