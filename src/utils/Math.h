#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

#include <cmath>

namespace ce {

/// \brief Return zero if value is within a non-inclusive threshold of zero
inline float Cutoff( float val, float threshold ) {
    if ( fabs( val ) < fabs( threshold ) ) {
        return 0.0f;
    }

    return val;
}

/// \brief Clip a value to a lower threshold
inline float Clip( float val, float threshold ) {
    if ( val < threshold ) {
        return threshold;
    }
    return val;
}

/// \brief Clamp a value to an upper threhsold
inline float Clamp( float val, float threshold ) {
    if ( val > threshold ) {
        return threshold;
    }
    return val;
}

}

#endif // MATH_H_INCLUDED
