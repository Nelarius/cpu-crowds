#include <core/Application.h>
#include <utils/Random.h>
#include <ppm/PPM.h>
#include <utils/ThreadPool.h>
#include <chrono>
#include <ratio>
#include <ctime>
#include <iostream>
#include <sstream>

using ce::Application;

Application::Application( const ObjectNode& node )
    :   grid_( node["grid"] ),
        dt_( node("time_delta") ),
        iterations_( std::uint32_t ( node("iterations") ) ) {
    //ctor
}

void Application::run() {
    initialize_();

    for ( std::uint32_t i = 0; i < iterations_; i++ ) {
        auto start = std::chrono::steady_clock::now();

        grid_.update();

        std::stringstream ss;
        ss << "density" << i << ".ppm";
        ce::WritePPM(
            grid_.getDensitySum(),
            grid_.getXNodeCount(),
            grid_.getYNodeCount(),
            ss.str(),
            ce::Mode::Density
        );

        auto end = std::chrono::steady_clock::now();
        auto tdiff = end - start;

        std::cout << "update " << i << " took " << std::chrono::duration<double, std::ratio<1,1>>(tdiff).count();
        std::cout << " seconds.\n";
    }



    ce::WritePPM(
        grid_.getDiscomfortSum(),
        grid_.getXNodeCount(),
        grid_.getYNodeCount(),
        "discomfort.ppm",
        ce::Mode::Discomfort
    );
}


void Application::initialize_() {
    ce::Randomize();
    grid_.initialize();
}
