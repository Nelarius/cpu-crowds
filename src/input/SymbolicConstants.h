#ifndef SYMBOLICCONSTANTS_H_INCLUDED
#define SYMBOLICCONSTANTS_H_INCLUDED

#include <cstdio>   //for EOF

namespace ce {

    //symbolic values for tokens
    const char number       = 'v';
    const char quit         = 'q';
    const char let          = 'L';
    const char name         = 'a';
    const char comment      = '#';  //a newline terminated comment
    const char openComment  = '[';  //a closeComment terminated comment
    const char closeComment = ']';
    const char eof          = 'e';
    //keywords
    const std::string declKey   = "let";    //declaration keyword
}

#endif // SYMBOLICCONSTANTS_H_INCLUDED
