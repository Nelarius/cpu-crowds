#ifndef THREADPOOL_H_INCLUDED
#define THREADPOOL_H_INCLUDED

#include <utils/thread/DLQueue.h>
#include <utils/Macro.h>
#include <memory>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <functional>

#include <iostream>

typedef std::queue<std::function<void()>> task_queue;

namespace ce {

/// \brief Use this class to schedule tasks to run in parallel.
class ThreadPool {
    public:
        ThreadPool();
        ThreadPool( std::size_t threads );
        ~ThreadPool();

        ThreadPool( ThreadPool&& )                  = delete;
        ThreadPool( const ThreadPool& )             = delete;
        ThreadPool& operator=( ThreadPool&& )       = delete;
        ThreadPool& operator=( const ThreadPool& )  = delete;

        /// \brief Initialize the ThreadPool with a number of threads.
        /// This method does nothing if the thread pool is already running,
        /// i.e. ThreadPool( unsigned ) was called.
        void initializeWithThreads( std::size_t threads );

        /// \brief Schedule a task to be executed by a thread immediately.
        void schedule( const std::function<void()>& );

        /// \brief Blocks until all tasks are safely completed. Use for synchronization.
        /// Use for synchronization.
        void waitUntilAllFinished() const;

        /// \brief Get the current number of threads.
        std::size_t threads() const;

    private:
        struct Worker {
            Worker( ThreadPool& owner )
                :   owner_( owner ),
                    thread_( [this]() -> void {
                    while( !owner_.stop_ ) {
                        std::function<void()> task;
                        bool first = true;
                        while ( owner_.taskCount_ == 0u && !owner_.stop_ ) {
                            // change the atomic value only the first time we enter this block
                            if( first ) {
                                first = false;
                                isProcessing_.store( false );
                            }
                            std::this_thread::yield();
                        }

                        bool wasPopped = false;
                        {   //acquire lock
                            std::unique_lock<std::mutex> lock( owner_.mutex_ );
                            if ( !owner_.taskQueue_.empty() ) {
                                wasPopped = true;
                                isProcessing_.store( true );
                                task = owner_.taskQueue_.front();
                                owner_.taskQueue_.pop();
                                owner_.taskCount_--;
                            }
                        }   //release lock

                        if ( wasPopped ) {
                            task();
                        }
                    }   //while
                    isFinished_.store( true );
                }),
                isProcessing_( false ),
                isFinished_( false )
                {}

            Worker( Worker&& )                  = delete;
            Worker& operator=( Worker&& )       = delete;
            Worker( const Worker& )             = delete;
            Worker& operator=( const Worker& )  = delete;

            ~Worker() { thread_.join(); }

            bool isProcessing() const { return isProcessing_; }

            bool isFinished() const { return isFinished_; }

            private:
                ThreadPool&         owner_;
                std::thread         thread_;
                std::atomic_bool    isProcessing_;
                std::atomic_bool    isFinished_;
        };  //Worker

        std::vector<std::unique_ptr<Worker>>    workers_;
        task_queue                              taskQueue_;
        std::atomic_uint                        taskCount_;
        std::mutex                              mutex_;
        std::condition_variable                 condition_;
        std::atomic_bool                        stop_;
};

}

#endif // THREADPOOL_H_INCLUDED
