#include <utils/ThreadPool.h>
#include <utils/Macro.h>
#include <chrono>
#include <iostream>
#include <chrono>

using ce::ThreadPool;

ThreadPool::ThreadPool()
    :   workers_(),
        taskQueue_(),
        taskCount_( 0u ),
        mutex_(),
        condition_(),
        stop_( false )
        {}

ThreadPool::ThreadPool( std::size_t threads ) : ThreadPool() {
    initializeWithThreads( threads );
}

ThreadPool::~ThreadPool() {
    stop_ = true;
    //condition_.notify_all();
    for ( auto& w: workers_ ) {
        while( !w->isFinished() );
    }
}

void ThreadPool::initializeWithThreads( std::size_t threads ) {
    for ( std::size_t i = 0; i < threads; i++ ) {
        workers_.push_back( std::unique_ptr<Worker>( new Worker(*this) ) );
    }   //for
}

void ThreadPool::schedule( const std::function<void()>& task ) {
    {
        std::unique_lock<std::mutex> lock( mutex_ );
        taskQueue_.push( task );
    }
    taskCount_++;
}

void ThreadPool::waitUntilAllFinished() const {
    for ( auto& w: workers_ ) {
        while( w->isProcessing() ) {
            std::this_thread::yield();
        }
    }
}

std::size_t ThreadPool::threads() const {
    return workers_.size();
}



