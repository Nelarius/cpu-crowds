#ifndef AGENT_H_INCLUDED
#define AGENT_H_INCLUDED

#include <utils/Macro.h>
#include <glm/glm.hpp>

#include <iostream>

namespace ce {

/// \brief This represents the agent on the navigation grid.
///
/// An agent should know its position, direction, speed and radius.
struct Agent {
    Agent(
        const glm::vec2& pos,
        const glm::vec2& dir,
        float rad
    ) : position( pos ), direction( dir ), speed( 1.0f ), radius( rad ) {}

    glm::vec2 position;
    glm::vec2 direction;
    float speed;
    float radius;

    float getDensity( float r ) {
        ASSERT( r < 2*radius, "Agent::getDensity> r is larger than 2*radius" );
        return 1.0 - 0.5 * r;
    }
};


}

#endif // AGENT_H_INCLUDED
