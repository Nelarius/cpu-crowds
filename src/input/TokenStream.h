#ifndef TOKENSTREAM_H
#define TOKENSTREAM_H

#include <input/Token.h>

#include <istream>
#include <queue>

namespace ce {

class TokenStream {
    public:
        TokenStream( std::istream& in );
        ~TokenStream();
        /// \brief Get the next token from the token stream.
        /// Throws std::exception.
        Token get();
        /// \brief Put a token back into the token stream.
        /// There is no limit on how many tokens can be placed back
        /// in the stream.
        void putback( const Token& t );

    private:
        //METHODS
        void checkComments_( char* ch );
        void eatComment_();
        void eatLineComment_();
        //FIELDS
        std::queue<Token>   buffer_;
        std::istream&       in_;
};

}

#endif // TOKENSTREAM_H
