# CPU crowds

This is my CPU implementation of the ideas presented in the work by [Treuille, Cooper, and Popovic](http://www.computerscience.nl/docs/vakken/mpap/papers/17.pdf) and its implementation by [Helmut Duregger's](https://cloud.github.com/downloads/hduregger/crowd/Simulation%20of%20large%20and%20dense%20crowds%20on%20the%20GPU%20using%20OpenCL.pdf) in his Master's thesis.

I'm writing this in order to explore the algorithms involved, and hopefully one day write a full-blown GPU implementation of them.

## Build

### Linux

Run `make` in the root folder. The resulting binary `crowds` will be created. It depends on the `variables` file, which specifies simulation inputs. A sample file is provided under `src/`
