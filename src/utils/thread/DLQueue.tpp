#include <utils/Macro.h>

namespace ce {

template<typename T>
DLQueue<T>::DLQueue() : _size( 0u ),
                        _head( nullptr ),
                        _tail( nullptr ),
                        _hmutex(),
                        _tmutex() {
    //we push to the tail
    //we pop from the head
    _head = std::shared_ptr<Node>( new Node( T() ) );   //dummy node
    _tail = _head;
}

template<typename T>
bool DLQueue<T>::empty() const {
    if ( _head->next == nullptr ) {
        return true;
    }
    return false;
}

template<typename T>
void DLQueue<T>::push( const T& t ) {
    auto node = std::shared_ptr<Node>( new Node( t ) );
    _size++;
    std::lock_guard<std::mutex> lock( _tmutex );
    {
        _tail->next = node; // after this operation, a pop would be valid when no objects in container
        _tail = node;
    }
}

template<typename T>
bool DLQueue<T>::pop( T& t ) {
    std::lock_guard<std::mutex> lock( _hmutex );
    {
        // if empty, don't pop
        if ( _head->next == nullptr ) {
            return false;
        }

        auto newHead = _head->next; //segfault occurs here
        _head = newHead;
        _size--;
        //the head gets discarded next time pop is called
        t = _head->element;
    }
    return true;
}

template<typename T>
size_t DLQueue<T>::size() const {
    return _size;
}

}   // namespace ce
