#ifndef SCRIPTPARSER_H
#define SCRIPTPARSER_H

#include <input/TokenStream.h>
#include <utils/Uncopyable.h>
#include <input/ObjectNode.h>

#include <map>
#include <istream>
#include <string>
#include <memory>

typedef std::map<const std::string, double>::const_iterator map_iterator;
typedef std::map<const std::string, double>                 variable_map;

namespace ce {

/// \brief A small interpreter for an input script.
///
/// To use the parser, create a ScriptParser object and pass the filestream to the constructor.
/// \code
/// std::fstream fin( script, std::fstream::in );
/// ScriptParser parser( fin );
/// parser.parse();
/// \endcode
///
/// Variables must be declared before using them for the first time.
/// Object members do not have to be declared. Simply use assignment
/// for creating object members. Note that all object members can be assigned
/// on the same line.
///
/// The grammar of the input script is as follows:
///
///Statement:
///     Declaration Newline
///	    Expression Newline
///	    Assignment Newline
///     ObjectDeclaration Newline
///     ObjectAssignment Newline
///	    Comment
///
///Newline
///	    "\n"
///
///Comment:
///	    # ... Newline
///	    [ .. ]
///
///Object
///     "{"
///     Assignment
///     ObjectAssignment
///     "}"
///
///ObjectDeclaration
///     "let" Name "=" Object
///
///Declaration:
///     "let" Name "=" Expression
///
///ObjectAssignment
///     Name "=" Object
///
///Assignment:
///	    Name "=" Expression
///
///Name:
///	    string of alphanumeric charaters starting with a letter
///
///Expression:
///	    Term
///	    Expression "+" Term
///	    Expression "-" Term
///     Expression "^" Term
///
///Term
///	    Primary
///	    Term "*" Primary
///	    Term "/" Primary
///     Term "^" Primary
///
///Primary:
///	    Variable
///     Constant
///	    Number
///	    "(" Expression ")"
///	    "-" Primary
///	    "+" Primary
///
///Number
///	    floating point literal
///
///Variable:
///	    string of alphanumeric characters which has been previously declared
class ScriptParser : private Uncopyable {
    public:
        ScriptParser( std::istream& in );
        ~ScriptParser() = default;

        /// \brief Parse input from the input stream
        void                parse();

        /// \brief Declare a variable with a name and a value.
        /// No checking is made as to whether the variable has already been defined.
        void                declare( const std::string& name, double value );

        /// \brief
        double              operator()( const std::string& name ) const;
        /// \brief
        const ObjectNode&   operator[]( const std::string& name ) const;

        /// \brief Returns the map which contains the variables.
        const variable_map& getVariables() const;
        /// \brief Returns the map containing the nodes.
        const node_map&     getNodes()  const;

    private:
        //METHODS:
        double  _statement();
        double  _declaration();
        double  _expression();
        double  _assignment();
        double  _term();
        double  _primary();
        void    _assignName( const std::string& name, double value );
        void    _defineName( const std::string& name, double value );
        // Check if the variable or object name has already been declared
        bool    _isDeclared( const std::string& name ) const;
        bool    _isNumber( const std::string& name ) const;
        bool    _isObject( const std::string& name ) const;

        node_ptr    _objectTraversal( const std::string& parent );
        void        _defineObject( const std::string&, node_ptr );
        void        _assignObject( const std::string&, node_ptr );

        //FIELDS:
        TokenStream     _tstream;
        std::istream&   _in;
        variable_map    _variables;
        node_map        _nodes;

};

}   //namespace ce

#endif // PARSER_H
