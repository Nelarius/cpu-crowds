#include <input/ScriptParser.h>
#include <utils/File.h>
#include <core/Application.h>
#include <utils/Random.h>
#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>    //for std::find()
#include <vector>
#include <cstdlib>      // for std::exit()

void PrintHelp() {
    std::cout << "Usage: crowd [arguments]\n\n";
    std::cout << "'--help' : Display help" << std::endl;
    std::cout << "'-f [string]' : Parameter file. The program looks for a file called 'variables' by default" << std::endl;
}

void ParseArguments( const std::vector<std::string>& args, std::string& file ) {
    //iterate through (option, value) pairs
    //it points to string
    for ( auto it = args.begin(); it != args.end(); it += 2 ) {
        if ( *it == "-f" ) {
            file = *(it + 1);
        } else {
            std::cout << "Unknown option: " << *it << std::endl;
            PrintHelp();
            std::exit(0);
        }
    }
}

int main( int argc, char** argv ) {
    // we skip the first argument, because it is the name of the program
    std::vector<std::string> args( argv+1, argv+argc );

    if ( std::find( args.begin(), args.end(), "--help" ) != args.end() ) {
        PrintHelp();
        return 0;
    }

    std::string file = "variables.txt";

    ParseArguments( args, file );

    if ( !ce::file_exists( file ) ) {
        std::cout << "The supplied parameter file does not exist" << std::endl;
        std::exit(0);
    }

    std::ifstream fin( file, std::ifstream::in );

    ce::ScriptParser parser( fin );

    parser.declare( "time_delta", 0.5 );
    parser.declare( "speed_min", 1.0 );
    parser.declare( "speed_max", 6.0 );
    parser.declare( "slope_min", 0.4 );
    parser.declare( "slope_max", 5.0 );
    parser.declare( "grid_x", 50.0 );
    parser.declare( "grid_y", 50.0 );
    parser.parse();

    ce::Application app( parser["application"] );
    app.run();

    return 0;
}
